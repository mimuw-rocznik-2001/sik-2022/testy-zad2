import traceback
import sys
from test_args import test_args
from test_invalid_messages import test_invalid_messages
from test_scores import test_scores
from test_scores_fragmented import test_scores_fragmented
from test_inputs import test_inputs
from test_kabooms import test_kabooms

tests = [
    test_args,
    test_invalid_messages,
    test_scores,
    test_scores_fragmented,
    test_inputs,
    test_kabooms,
]

if __name__ == '__main__':
    for test in tests:
        print(test.__name__ + '... ', end='')
        sys.stdout.flush()
        try:
            test()
        except Exception as ex:
            print('ERROR')
            print('-------------------------------------------')
            print(traceback.format_exc())
        else:
            print('OK')
