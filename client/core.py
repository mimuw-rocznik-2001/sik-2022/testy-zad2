import socket
from contextlib import closing
import subprocess
import time
import sys
import random

CLIENT_PATH = '../robots-client'
SEED = 42


def find_free_port():
    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as s:
        s.bind(('', 0))
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        return s.getsockname()[1]


def read_exactly(sock, n):
    buff = bytearray(n)
    pos = 0
    while pos < n:
        cr = sock.recv_into(memoryview(buff)[pos:])
        if cr == 0:
            raise EOFError
        pos += cr
    return buff


class Sandbox:
    def __init__(self, localhost_addr='localhost', player_name='testowy',
                 max_server_packet_size=None):
        self.gui_socket = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
        self.gui_socket.bind(('', 0))
        self.gui_socket.settimeout(0.5)
        self.gui_port = self.gui_socket.getsockname()[1]

        server_listen_socket = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
        server_listen_socket.bind(('', 0))
        server_listen_socket.listen()
        server_listen_socket.settimeout(1)
        server_port = server_listen_socket.getsockname()[1]

        if "-d" not in sys.argv:
            popen_kwargs = {
                'stdout': subprocess.DEVNULL,
                'stderr': subprocess.DEVNULL,
            }
        else:
            popen_kwargs = {}

        self.client_port = find_free_port()
        self.client = subprocess.Popen([
            CLIENT_PATH, '-d', '{}:{}'.format(localhost_addr, self.gui_port),
            '-n', player_name, '-p', str(self.client_port), '-s',
            '{}:{}'.format(localhost_addr, server_port)
        ], **popen_kwargs)
        time.sleep(0.5)

        self.server_socket, _ = server_listen_socket.accept()
        self.server_socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        self.server_socket.settimeout(0.5)

        self.max_server_packet_size = max_server_packet_size
        random.seed(SEED)

    def __del__(self):
        self.client.terminate()
        self.client.communicate()

    def gui_send(self, msg):
        self.gui_socket.sendto(msg[0], ('localhost', self.client_port))

    def gui_receive(self, msg=None):
        received = self.gui_socket.recv(2048)
        assert (not msg) or (received in msg)

    def server_send(self, msg):
        if not self.max_server_packet_size:
            self.server_socket.sendall(msg[0])
        else:
            sent = 0
            while sent < len(msg[0]):
                packet_size = random.randint(1, min(self.max_server_packet_size, len(msg[0]) - sent))
                self.server_socket.sendall(msg[0][sent:sent+packet_size])
                sent += packet_size


    def server_receive(self, msg):
        received = read_exactly(self.server_socket, len(msg[0]))
        assert (not msg) or (received in msg)

    def server_do_not_receive_anything(self):
        try:
            read_exactly(self.server_socket, 1)
        except (TimeoutError, socket.timeout):
            pass
        else:
            assert False

    def gui_do_not_receive_anything(self):
        try:
            self.gui_receive()
        except (TimeoutError, socket.timeout):
            pass
        else:
            assert False

    def client_terminated(self):
        assert self.client.poll() is not None
        assert self.client.returncode != 0
