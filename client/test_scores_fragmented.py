from core import Sandbox
from test_scores import test_scores


def test_scores_fragmented():
    player_name = 'Dipsy'
    s = Sandbox(player_name=player_name, max_server_packet_size=5)
    test_scores(s)


if __name__ == '__main__':
    test_scores_fragmented()