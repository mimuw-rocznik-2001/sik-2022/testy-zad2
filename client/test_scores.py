from core import Sandbox
from formats import *


'''
P: Obliczanie score w kliencie to nie jest tak proste, że się sprawdza ile razy
przyszedł komunikat o zabiciu gracza, tylko score to ilość tur, gdzie występuje
przynajmniej jeden taki komunikat?
O: Tak
'''

'''
P: Co powinniśmy zrobić po otrzymaniu od serwera GameStarted? Wysłać do GUI Lobby,
wysłać do GUI Game, czy nic nie wysyłać do GUI?
O: Nic. Game dopiero po pierwszym Turn.
'''


def test_scores(sandbox=None):
    player_name = 'danon'
    s = sandbox or Sandbox(player_name=player_name)
    game_kwargs = {
        'server_name': 'testowy',
        'size_x': 4,
        'size_y': 4,
        'game_length': 4,
    }
    hello_kwargs = {
        **game_kwargs,
        'players_count': 2,
        'explosion_radius': 2,
        'bomb_timer': 1
    }
    players = {
        1: Player('alice', 'hej.pl:44'),
        0: Player('bob', '::1:55'),
    }

    s.server_send(Hello(**hello_kwargs))
    s.gui_receive(Lobby(**hello_kwargs, players={}))
    s.server_send(GameStarted(players=players))
    s.gui_do_not_receive_anything()
    s.server_send(Turn(0, [
        PlayerMoved(0, Position(0, 0)),
        PlayerMoved(1, Position(0, 1)),
        BlockPlaced(Position(0, 2)),
        BlockPlaced(Position(1, 0)),
    ]))
    s.gui_receive(Game(
        **game_kwargs,
        turn=0,
        players=players,
        player_positions={
            0: Position(0, 0),
            1: Position(0, 1),
        },
        blocks=[
            Position(1, 0),
            Position(0, 2),
        ],
        bombs=[],
        explosions=[],
        scores={
            0: pack_u32(0),
            1: pack_u32(0),
        }
    ))
    s.server_send(Turn(1, [
        BombPlaced(4, Position(0, 0)),
        BombPlaced(2, Position(0, 1)),
    ]))
    s.gui_receive(Game(
        **game_kwargs,
        turn=1,
        players=players,
        player_positions={
            0: Position(0, 0),
            1: Position(0, 1),
        },
        blocks=[
            Position(1, 0),
            Position(0, 2),
        ],
        bombs=[
            Bomb(Position(0, 0), 1),
            Bomb(Position(0, 1), 1),
        ],
        explosions=[],
        scores={
            0: pack_u32(0),
            1: pack_u32(0),
        }
    ))
    s.server_send(Turn(2, [
        BombExploded(id=4, robots_destroyed=[pack_u8(0), pack_u8(1)], blocks_destroyed=[Position(0, 2), Position(1, 0)]),
        BombExploded(id=2, robots_destroyed=[pack_u8(1), pack_u8(0)], blocks_destroyed=[Position(0, 2)]),
        PlayerMoved(0, Position(2, 2)),
        PlayerMoved(1, Position(3, 0))
    ]))
    s.gui_receive(
        Game(
            **game_kwargs,
            turn=2,
            players={
                1: Player('alice', 'hej.pl:44'),
                0: Player('bob', '::1:55'),
            },
            player_positions={
                0: Position(2, 2),
                1: Position(3, 0),
            },
            blocks={},
            bombs={},
            explosions=[
                Position(0, 0),
                Position(0, 1),
                Position(0, 2),
                Position(1, 0),
                Position(1, 1),
                Position(2, 1),
            ],
            scores={
                1: pack_u32(1),
                0: pack_u32(1)
            }
        )
    )


if __name__ == '__main__':
    test_scores()
