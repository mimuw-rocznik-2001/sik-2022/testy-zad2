from core import Sandbox
from formats import *


'''
"W komunikatach wszystkie liczby przesyłane są w sieciowej kolejności bajtów,
a wszystkie napisy muszą być zakodowane w UTF-8 i mieć długość krótszą niż 256 bajtów."
"Programy powinny umożliwiać komunikację zarówno przy użyciu IPv4, jak i IPv6."
'''


def _test_starting(server_name='Serwer testowy', player_name='gracz', localhost_addr='localhost'):
    s = Sandbox(localhost_addr=localhost_addr, player_name=player_name)
    common_kwargs = {
        'server_name': server_name,
        'players_count': 3,
        'size_x': 4,
        'size_y': 4,
        'game_length': 2,
        'explosion_radius': 2,
        'bomb_timer': 1
    }
    s.server_send(Hello(**common_kwargs))
    s.gui_receive(Lobby(**common_kwargs, players={}))
    s.gui_send(InputPlaceBlock())
    s.server_receive(Join(name=player_name))


def test_args():
    utf8_server_name = '😆😆😆'
    utf8_player_name = '🙃🙃🙃'
    localhost_addrs = [
        'localhost',
        '127.0.0.1',
        '::1',
        '0000:0000:0000:0000:0000:0000:0000:0001',
    ]
    for addr in localhost_addrs:
        _test_starting(utf8_server_name, utf8_player_name, addr)


if __name__ == '__main__':
    test_args()
