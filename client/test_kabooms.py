from core import Sandbox
from formats import *


def test_kabooms():
    player_name = 'danon'
    s = Sandbox(player_name=player_name)
    game_kwargs = {
        'server_name': 'testowy',
        'size_x': 2,
        'size_y': 4,
        'game_length': 10,
    }
    hello_kwargs = {
        **game_kwargs,
        'players_count': 2,
        'explosion_radius': 1,
        'bomb_timer': 2
    }
    players = {
        0: Player('anon', '127.0.0.1:2115'),
        1: Player('danon', '127.0.0.1:2116'),
    }
    game_kwargs['players'] = players
    game_kwargs['player_positions'] = {
        0: Position(1, 1),
        1: Position(1, 1),
    }

    s.server_send(Hello(**hello_kwargs))
    s.gui_receive(Lobby(**hello_kwargs, players={}))
    s.server_send(GameStarted(players=players))
    s.gui_do_not_receive_anything()

    s.server_send(Turn(0, [
        PlayerMoved(0, Position(1, 1)),
        PlayerMoved(1, Position(1, 1)),
        BlockPlaced(Position(1, 1)),
        BlockPlaced(Position(1, 0)),
    ]))
    s.gui_receive(Game(
        **game_kwargs,
        turn=0,
        blocks=[
            Position(1, 1),
            Position(1, 0),
        ],
        bombs=[],
        explosions=[],
        scores={
            0: pack_u32(0),
            1: pack_u32(0),
        }
    ))

    s.server_send(Turn(1, [
        BombPlaced(0, Position(1, 1)),
        BombPlaced(1, Position(1, 1)),
    ]))
    s.gui_receive(Game(
        **game_kwargs,
        turn=1,
        blocks=[
            Position(1, 1),
            Position(1, 0),
        ],
        bombs=[
            Bomb(Position(1, 1), 2),
            Bomb(Position(1, 1), 2),
        ],
        explosions=[],
        scores={
            0: pack_u32(0),
            1: pack_u32(0),
        }
    ))

    s.server_send(Turn(2, [
        BombPlaced(2, Position(1, 1)),
        BombPlaced(3, Position(1, 1)),
    ]))
    s.gui_receive(Game(
        **game_kwargs,
        turn=2,
        blocks=[
            Position(1, 1),
            Position(1, 0),
        ],
        bombs=[
            Bomb(Position(1, 1), 2),
            Bomb(Position(1, 1), 2),
            Bomb(Position(1, 1), 1),
            Bomb(Position(1, 1), 1),
        ],
        explosions=[],
        scores={
            0: pack_u32(0),
            1: pack_u32(0),
        }
    ))

    s.server_send(Turn(3, [
        BombExploded(
            id=0,
            robots_destroyed=[pack_u8(0), pack_u8(1)],
            blocks_destroyed=[Position(1, 1)],
        ),
        BombExploded(
            id=1,
            robots_destroyed=[pack_u8(0), pack_u8(1)],
            blocks_destroyed=[Position(1, 1)],
        ),
        PlayerMoved(0, Position(1, 2)),
        PlayerMoved(1, Position(1, 3)),
    ]))
    game_kwargs['player_positions'] = {
        0: Position(1, 2),
        1: Position(1, 3),
    }
    s.gui_receive(Game(
        **game_kwargs,
        turn=3,
        blocks=[
            Position(1, 0),
        ],
        bombs=[
            Bomb(Position(1, 1), 1),
            Bomb(Position(1, 1), 1),
        ],
        explosions=[
            Position(1, 1),
        ],
        scores={
            0: pack_u32(1),
            1: pack_u32(1),
        }
    ))

    s.server_send(Turn(4, [
        BombExploded(
            id=2,
            robots_destroyed=[],
            blocks_destroyed=[Position(1, 0)],
        ),
        BombExploded(
            id=3,
            robots_destroyed=[],
            blocks_destroyed=[Position(1, 0)],
        ),
    ]))
    s.gui_receive(Game(
        **game_kwargs,
        turn=4,
        blocks=[
        ],
        bombs=[
        ],
        explosions=[
            Position(1, 1),
            Position(1, 0),
            Position(1, 2),
            Position(0, 1),
        ],
        scores={
            0: pack_u32(1),
            1: pack_u32(1),
        }
    ))



if __name__ == '__main__':
    test_kabooms()
