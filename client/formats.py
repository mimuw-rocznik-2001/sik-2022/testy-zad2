import itertools
from enum import IntEnum


def combine(*args, **kwargs):
    return [b''.join(c) for c in itertools.product(*args, **kwargs)]


def pack_u8(val):
    return [int(val).to_bytes(1, 'big')]


def pack_u16(val):
    return [int(val).to_bytes(2, 'big')]


def pack_u32(val):
    return [int(val).to_bytes(4, 'big')]


def pack_str(s):
    b = s.encode('utf-8')
    return [int(len(b)).to_bytes(1, 'big') + b]


def pack_list(l):
    res = []
    for perm in itertools.permutations(l):
        res += combine(pack_u32(len(l)), combine(*perm))
    return res


def pack_dict(d):
    res = []
    for perm in itertools.permutations(d.items()):
        res += [int(len(d)).to_bytes(4, 'big') + b''.join([id.to_bytes(1, 'big') + val[0] for id, val in perm])]
    return res


def Player(name, address):
    return combine(
        pack_str(name),
        pack_str(address),
    )


def Position(x, y):
    return combine(
        pack_u16(x),
        pack_u16(y)
    )


def Bomb(position, timer):
    return combine(
        position,
        pack_u16(timer),
    )


class Direction(IntEnum):
    UP = 0
    RIGHT = 1
    DOWN = 2
    LEFT = 3


def pack_dir(direction):
    return pack_u8(int(direction))


# --------------------- Events -------------------

def BombPlaced(id, position):
    return combine(
        pack_u8(0),
        pack_u32(id),
        position
    )


def BombExploded(id, robots_destroyed, blocks_destroyed):
    return combine(
        pack_u8(1),
        pack_u32(id),
        pack_list(robots_destroyed),
        pack_list(blocks_destroyed)
    )


def PlayerMoved(id, position):
    return combine(
        pack_u8(2),
        pack_u8(id),
        position
    )


def BlockPlaced(position):
    return combine(
        pack_u8(3),
        position
    )


# --------------------- Server messages -------------------

def Hello(server_name, players_count, size_x, size_y, game_length, explosion_radius, bomb_timer):
    return combine(
        pack_u8(0),
        pack_str(server_name),
        pack_u8(players_count),
        pack_u16(size_x),
        pack_u16(size_y),
        pack_u16(game_length),
        pack_u16(explosion_radius),
        pack_u16(bomb_timer),
    )


def AcceptedPlayer(id, player):
    return combine(
        pack_u8(1),
        pack_u8(id),
        player
    )


def GameStarted(players):
    return combine(
        pack_u8(2),
        pack_dict(players)
    )


def Turn(turn, events):
    return combine(
        pack_u8(3),
        pack_u16(turn),
        pack_list(events)
    )


def GameEnded(scores):
    return combine(
        pack_u8(4),
        pack_dict(scores)
    )


# --------------------- Client messages -------------------

def Join(name):
    return combine(
        pack_u8(0),
        pack_str(name)
    )


def PlaceBomb():
    return pack_u8(1)


def PlaceBlock():
    return pack_u8(2)


def Move(direction):
    return combine(
        pack_u8(3),
        pack_dir(direction)
    )


# --------------------- Display messages -------------------

def Lobby(server_name, players_count, size_x, size_y, game_length, explosion_radius, bomb_timer, players):
    return combine(
        Hello(server_name, players_count, size_x, size_y, game_length, explosion_radius, bomb_timer),
        pack_dict(players),
    )


def Game(server_name, size_x, size_y, game_length, turn, players, player_positions,
         blocks, bombs, explosions, scores):
    return combine(
        pack_u8(1),
        pack_str(server_name),
        pack_u16(size_x),
        pack_u16(size_y),
        pack_u16(game_length),
        pack_u16(turn),
        pack_dict(players),
        pack_dict(player_positions),
        pack_list(blocks),
        pack_list(bombs),
        pack_list(explosions),
        pack_dict(scores),
    )


# --------------------- Input messages -------------------

def InputPlaceBomb():
    return pack_u8(0)


def InputPlaceBlock():
    return pack_u8(1)


def InputMove(direction):
    return combine(
        pack_u8(2),
        pack_dir(direction)
    )
