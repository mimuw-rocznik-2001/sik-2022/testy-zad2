from core import Sandbox
from formats import *
import random
import time


'''
"Klient wysyła komunikat Join do serwera po otrzymaniu dowolnego (poprawnego) komunikatu od GUI"
P: Co zrobić, gdy GUI wyśle komunikat, którego nie da się sparsować, do klienta?
O: Zignorować
P: Co zrobić, gdy serwer wyśle komunikat, którego nie da się sparsować, do klienta?
O: Rozłączyć się, bo po niepoprawnym komunikacie nie wiadomo, kiedy miałby zacząć się poprawny komunikat
'''


SEED = 42


def test_invalid_messages():
    player_name = 'danon'

    random.seed(SEED)
    s = Sandbox(player_name=player_name)

    common_kwargs = {
        'server_name': 'testowy',
        'players_count': 3,
        'size_x': 4,
        'size_y': 4,
        'game_length': 2,
        'explosion_radius': 2,
        'bomb_timer': 1
    }
    s.server_send(Hello(**common_kwargs))
    s.gui_receive(Lobby(**common_kwargs, players={}))

    for _ in range(10):
        len = random.randint(0, 5)
        s.gui_send(combine(*(pack_u8(random.randint(3, 255)) for _ in range(len))))
    s.gui_send(combine(InputPlaceBlock(), pack_u8(4)))

    s.server_do_not_receive_anything()

    s.gui_send(InputPlaceBlock())
    s.server_receive(Join(name=player_name))

    s.server_send(pack_u8(10))
    time.sleep(0.5)
    s.client_terminated()


if __name__ == '__main__':
    test_invalid_messages()
