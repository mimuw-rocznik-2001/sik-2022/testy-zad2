from core import Sandbox
from formats import *
import time

'''
P: Co powinniśmy zrobić po otrzymaniu od serwera GameStarted? Wysłać do GUI Lobby,
wysłać do GUI Game, czy nic nie wysyłać do GUI?
O: Nic. Game dopiero po pierwszym Turn.
'''

def test_inputs():
    player_name = 'Tinky winky'
    s = Sandbox(player_name=player_name)
    common_kwargs = {
        'server_name': 'testowy',
        'players_count': 1,
        'size_x': 4,
        'size_y': 4,
        'game_length': 1,
        'explosion_radius': 2,
        'bomb_timer': 1
    }
    s.server_send(Hello(**common_kwargs))
    s.gui_receive(Lobby(**common_kwargs, players={}))

    for i in range(3):
        s.gui_send(InputPlaceBlock())
        s.server_receive(Join(name=player_name))
        s.server_send(AcceptedPlayer(0, Player('Tinky winky', 'Teletubbyland')))
        s.gui_receive()
        s.server_send(GameStarted(players={
            0: Player('Tinky winky', 'Teletubbyland')
        }))
        s.gui_do_not_receive_anything()
        s.server_send(Turn(0, [
            PlayerMoved(0, Position(3, 2)),
        ]))
        s.gui_receive()

        for _ in range(3):
            s.gui_send(InputPlaceBlock())
            s.server_receive(PlaceBlock())
            s.gui_send(InputPlaceBomb())
            s.server_receive(PlaceBomb())
            s.gui_send(InputMove(Direction.UP))
            s.server_receive(Move(Direction.UP))

        s.server_send(Turn(1, []))
        s.gui_receive()
        s.server_send(GameEnded({
            0: pack_u32(0)
        }))
        s.gui_receive()
        if i < 2:
            time.sleep(0.25)


if __name__ == '__main__':
    test_inputs()
