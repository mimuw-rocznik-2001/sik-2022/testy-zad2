# Poniższy skrypt nie jest do uruchamiania, tylko po to, aby wyczytać konfigurację z uruchomionych serwerów.
function run_server() {
	mkdir /tmp/robots-server-logs/
	nohup ./robots-server $@ > "/tmp/robots-server-logs/port$2" 2>&1 &
}
pkill robots-server

echo 'Adres serwera to students.mimuw.edu.pl, porty tak jak niżej'
echo 'Logi są w /tmp/robots-server-logs/ na studentsie, można je nasłuchiwać komendą tail -f <filename>'

run_server --port 10000 --server-name 'Testowy jednograczowy_00, krótkie gry,_wolne_tury' --players-count 1 --turn-duration 1000 --game-length 10 --size-x 4 --size-y 4 --explosion-radius 2 --bomb-timer 3 --initial-blocks 6
run_server --port 10001 --server-name 'Testowy jednograczowy_01, krótkie gry,_wolne_tury' --players-count 1 --turn-duration 1000 --game-length 10 --size-x 4 --size-y 4 --explosion-radius 2 --bomb-timer 3 --initial-blocks 6
run_server --port 10002 --server-name 'Testowy jednograczowy_02, krótkie gry,_wolne_tury' --players-count 1 --turn-duration 1000 --game-length 10 --size-x 4 --size-y 4 --explosion-radius 2 --bomb-timer 3 --initial-blocks 6
run_server --port 10003 --server-name 'Testowy jednograczowy_03, krótkie gry,_wolne_tury' --players-count 1 --turn-duration 1000 --game-length 10 --size-x 4 --size-y 4 --explosion-radius 2 --bomb-timer 3 --initial-blocks 6
run_server --port 10004 --server-name 'Testowy jednograczowy_04, krótkie gry,_wolne_tury' --players-count 1 --turn-duration 1000 --game-length 10 --size-x 4 --size-y 4 --explosion-radius 2 --bomb-timer 3 --initial-blocks 6
run_server --port 10005 --server-name 'Testowy_jednograczowy_05,_krótkie_gry,_wolne_tury' --players-count 1 --turn-duration 1000 --game-length 10 --size-x 4 --size-y 4 --explosion-radius 2 --bomb-timer 3 --initial-blocks 6
run_server --port 10006 --server-name 'Testowy_jednograczowy_06,_krótkie_gry,_wolne_tury' --players-count 1 --turn-duration 1000 --game-length 10 --size-x 4 --size-y 4 --explosion-radius 2 --bomb-timer 3 --initial-blocks 6
run_server --port 10007 --server-name 'Testowy_jednograczowy_07,_krótkie_gry,_wolne_tury' --players-count 1 --turn-duration 1000 --game-length 10 --size-x 4 --size-y 4 --explosion-radius 2 --bomb-timer 3 --initial-blocks 6
run_server --port 10008 --server-name 'Testowy_jednograczowy_08,_krótkie_gry,_wolne_tury' --players-count 1 --turn-duration 1000 --game-length 10 --size-x 4 --size-y 4 --explosion-radius 2 --bomb-timer 3 --initial-blocks 6
run_server --port 10009 --server-name 'Testowy_jednograczowy_09,_krótkie_gry,_wolne_tury' --players-count 1 --turn-duration 1000 --game-length 10 --size-x 4 --size-y 4 --explosion-radius 2 --bomb-timer 3 --initial-blocks 6
run_server --port 10010 --server-name 'Testowy jednograczowy_10, krótkie gry,_wolne_tury' --players-count 1 --turn-duration 1000 --game-length 10 --size-x 4 --size-y 4 --explosion-radius 2 --bomb-timer 3 --initial-blocks 6
run_server --port 10011 --server-name 'Testowy jednograczowy_11, krótkie gry,_wolne_tury' --players-count 1 --turn-duration 1000 --game-length 10 --size-x 4 --size-y 4 --explosion-radius 2 --bomb-timer 3 --initial-blocks 6
run_server --port 10012 --server-name 'Testowy jednograczowy_12, krótkie gry,_wolne_tury' --players-count 1 --turn-duration 1000 --game-length 10 --size-x 4 --size-y 4 --explosion-radius 2 --bomb-timer 3 --initial-blocks 6
run_server --port 10013 --server-name 'Testowy jednograczowy_13, krótkie gry,_wolne_tury' --players-count 1 --turn-duration 1000 --game-length 10 --size-x 4 --size-y 4 --explosion-radius 2 --bomb-timer 3 --initial-blocks 6
run_server --port 10014 --server-name 'Testowy jednograczowy_14, krótkie gry,_wolne_tury' --players-count 1 --turn-duration 1000 --game-length 10 --size-x 4 --size-y 4 --explosion-radius 2 --bomb-timer 3 --initial-blocks 6
run_server --port 10015 --server-name 'Testowy_jednograczowy_15,_krótkie_gry,_wolne_tury' --players-count 1 --turn-duration 1000 --game-length 10 --size-x 4 --size-y 4 --explosion-radius 2 --bomb-timer 3 --initial-blocks 6
run_server --port 10016 --server-name 'Testowy_jednograczowy_16,_krótkie_gry,_wolne_tury' --players-count 1 --turn-duration 1000 --game-length 10 --size-x 4 --size-y 4 --explosion-radius 2 --bomb-timer 3 --initial-blocks 6
run_server --port 10017 --server-name 'Testowy_jednograczowy_17,_krótkie_gry,_wolne_tury' --players-count 1 --turn-duration 1000 --game-length 10 --size-x 4 --size-y 4 --explosion-radius 2 --bomb-timer 3 --initial-blocks 6
run_server --port 10018 --server-name 'Testowy_jednograczowy_18,_krótkie_gry,_wolne_tury' --players-count 1 --turn-duration 1000 --game-length 10 --size-x 4 --size-y 4 --explosion-radius 2 --bomb-timer 3 --initial-blocks 6
run_server --port 10019 --server-name 'Testowy_jednograczowy_19,_krótkie_gry,_wolne_tury' --players-count 1 --turn-duration 1000 --game-length 10 --size-x 4 --size-y 4 --explosion-radius 2 --bomb-timer 3 --initial-blocks 6


run_server --port 10100 --server-name 'Jednograczowy_0,_wolne_tury'   --players-count 1 --turn-duration 1000 --game-length 200 --size-x 4 --size-y 4 --explosion-radius 3 --bomb-timer 5 --initial-blocks 6
run_server --port 10101 --server-name 'Jednograczowy_1,_wolne_tury'   --players-count 1 --turn-duration 1000 --game-length 200 --size-x 4 --size-y 4 --explosion-radius 3 --bomb-timer 5 --initial-blocks 6
run_server --port 10102 --server-name 'Jednograczowy_2,_wolne_tury'   --players-count 1 --turn-duration 1000 --game-length 200 --size-x 4 --size-y 4 --explosion-radius 3 --bomb-timer 5 --initial-blocks 6

run_server --port 10110 --server-name 'Jednograczowy_0,_średnie_tury' --players-count 1 --turn-duration  400 --game-length 200 --size-x 4 --size-y 4 --explosion-radius 3 --bomb-timer 6 --initial-blocks 6
run_server --port 10111 --server-name 'Jednograczowy_1,_średnie_tury' --players-count 1 --turn-duration  400 --game-length 200 --size-x 4 --size-y 4 --explosion-radius 3 --bomb-timer 6 --initial-blocks 6
run_server --port 10112 --server-name 'Jednograczowy_2,_średnie_tury' --players-count 1 --turn-duration  400 --game-length 200 --size-x 4 --size-y 4 --explosion-radius 3 --bomb-timer 6 --initial-blocks 6

run_server --port 10120 --server-name 'Jednograczowy_0,_szybkie_tury' --players-count 1 --turn-duration  100 --game-length 750 --size-x 4 --size-y 4 --explosion-radius 3 --bomb-timer 15 --initial-blocks 6
run_server --port 10121 --server-name 'Jednograczowy_1,_szybkie_tury' --players-count 1 --turn-duration  100 --game-length 750 --size-x 4 --size-y 4 --explosion-radius 3 --bomb-timer 15 --initial-blocks 6
run_server --port 10122 --server-name 'Jednograczowy_2,_szybkie_tury' --players-count 1 --turn-duration  100 --game-length 750 --size-x 4 --size-y 4 --explosion-radius 3 --bomb-timer 15 --initial-blocks 6


run_server --port 10200 --server-name 'Dwugraczowy_0,_wolne_tury'   --players-count 2 --turn-duration 1000 --game-length 200 --size-x 5 --size-y 5 --explosion-radius 3 --bomb-timer 5 --initial-blocks 6
run_server --port 10201 --server-name 'Dwugraczowy_1,_wolne_tury'   --players-count 2 --turn-duration 1000 --game-length 200 --size-x 5 --size-y 5 --explosion-radius 3 --bomb-timer 5 --initial-blocks 6
run_server --port 10202 --server-name 'Dwugraczowy_2,_wolne_tury'   --players-count 2 --turn-duration 1000 --game-length 200 --size-x 5 --size-y 5 --explosion-radius 3 --bomb-timer 5 --initial-blocks 6

run_server --port 10210 --server-name 'Dwugraczowy_0,_średnie_tury' --players-count 2 --turn-duration  400 --game-length 200 --size-x 5 --size-y 5 --explosion-radius 3 --bomb-timer 6 --initial-blocks 6
run_server --port 10211 --server-name 'Dwugraczowy_1,_średnie_tury' --players-count 2 --turn-duration  400 --game-length 200 --size-x 5 --size-y 5 --explosion-radius 3 --bomb-timer 6 --initial-blocks 6
run_server --port 10212 --server-name 'Dwugraczowy_2,_średnie_tury' --players-count 2 --turn-duration  400 --game-length 200 --size-x 5 --size-y 5 --explosion-radius 3 --bomb-timer 6 --initial-blocks 6

run_server --port 10220 --server-name 'Dwugraczowy_0,_szybkie_tury' --players-count 2 --turn-duration  100 --game-length 750 --size-x 5 --size-y 5 --explosion-radius 3 --bomb-timer 15 --initial-blocks 6
run_server --port 10221 --server-name 'Dwugraczowy_1,_szybkie_tury' --players-count 2 --turn-duration  100 --game-length 750 --size-x 5 --size-y 5 --explosion-radius 3 --bomb-timer 15 --initial-blocks 6
run_server --port 10222 --server-name 'Dwugraczowy_2,_szybkie_tury' --players-count 2 --turn-duration  100 --game-length 750 --size-x 5 --size-y 5 --explosion-radius 3 --bomb-timer 15 --initial-blocks 6


run_server --port 10300 --server-name 'Trzygraczowy_0,_wolne_tury'   --players-count 3 --turn-duration 1000 --game-length 200 --size-x 6 --size-y 6 --explosion-radius 3 --bomb-timer 5 --initial-blocks 6
run_server --port 10301 --server-name 'Trzygraczowy_1,_wolne_tury'   --players-count 3 --turn-duration 1000 --game-length 200 --size-x 6 --size-y 6 --explosion-radius 3 --bomb-timer 5 --initial-blocks 6
run_server --port 10302 --server-name 'Trzygraczowy_2,_wolne_tury'   --players-count 3 --turn-duration 1000 --game-length 200 --size-x 6 --size-y 6 --explosion-radius 3 --bomb-timer 5 --initial-blocks 6

run_server --port 10310 --server-name 'Trzygraczowy_0,_średnie_tury' --players-count 3 --turn-duration  400 --game-length 200 --size-x 6 --size-y 6 --explosion-radius 3 --bomb-timer 6 --initial-blocks 6
run_server --port 10311 --server-name 'Trzygraczowy_1,_średnie_tury' --players-count 3 --turn-duration  400 --game-length 200 --size-x 6 --size-y 6 --explosion-radius 3 --bomb-timer 6 --initial-blocks 6
run_server --port 10312 --server-name 'Trzygraczowy_2,_średnie_tury' --players-count 3 --turn-duration  400 --game-length 200 --size-x 6 --size-y 6 --explosion-radius 3 --bomb-timer 6 --initial-blocks 6

run_server --port 10320 --server-name 'Trzygraczowy_0,_szybkie_tury' --players-count 3 --turn-duration  100 --game-length 750 --size-x 6 --size-y 6 --explosion-radius 3 --bomb-timer 15 --initial-blocks 6
run_server --port 10321 --server-name 'Trzygraczowy_1,_szybkie_tury' --players-count 3 --turn-duration  100 --game-length 750 --size-x 6 --size-y 6 --explosion-radius 3 --bomb-timer 15 --initial-blocks 6
run_server --port 10322 --server-name 'Trzygraczowy_2,_szybkie_tury' --players-count 3 --turn-duration  100 --game-length 750 --size-x 6 --size-y 6 --explosion-radius 3 --bomb-timer 15 --initial-blocks 6


run_server --port 10400 --server-name 'Czterograczowy_0,_wolne_tury'   --players-count 4 --turn-duration 1000 --game-length 200 --size-x 8 --size-y 8 --explosion-radius 4 --bomb-timer 5 --initial-blocks 10
run_server --port 10401 --server-name 'Czterograczowy_1,_wolne_tury'   --players-count 4 --turn-duration 1000 --game-length 200 --size-x 8 --size-y 8 --explosion-radius 4 --bomb-timer 5 --initial-blocks 10
run_server --port 10402 --server-name 'Czterograczowy_2,_wolne_tury'   --players-count 4 --turn-duration 1000 --game-length 200 --size-x 8 --size-y 8 --explosion-radius 4 --bomb-timer 5 --initial-blocks 10

run_server --port 10410 --server-name 'Czterograczowy_0,_średnie_tury' --players-count 4 --turn-duration  400 --game-length 200 --size-x 8 --size-y 8 --explosion-radius 4 --bomb-timer 6 --initial-blocks 10
run_server --port 10411 --server-name 'Czterograczowy_1,_średnie_tury' --players-count 4 --turn-duration  400 --game-length 200 --size-x 8 --size-y 8 --explosion-radius 4 --bomb-timer 6 --initial-blocks 10
run_server --port 10412 --server-name 'Czterograczowy_2,_średnie_tury' --players-count 4 --turn-duration  400 --game-length 200 --size-x 8 --size-y 8 --explosion-radius 4 --bomb-timer 6 --initial-blocks 10

run_server --port 10420 --server-name 'Czterograczowy_0,_szybkie_tury' --players-count 4 --turn-duration  100 --game-length 750 --size-x 8 --size-y 8 --explosion-radius 4 --bomb-timer 15 --initial-blocks 10
run_server --port 10421 --server-name 'Czterograczowy_1,_szybkie_tury' --players-count 4 --turn-duration  100 --game-length 750 --size-x 8 --size-y 8 --explosion-radius 4 --bomb-timer 15 --initial-blocks 10
run_server --port 10422 --server-name 'Czterograczowy_2,_szybkie_tury' --players-count 4 --turn-duration  100 --game-length 750 --size-x 8 --size-y 8 --explosion-radius 4 --bomb-timer 15 --initial-blocks 10


run_server --port 10500 --server-name 'Pięciograczowy_0,_wolne_tury'   --players-count 5 --turn-duration 1000 --game-length 200 --size-x 9 --size-y 9 --explosion-radius 4 --bomb-timer 5 --initial-blocks 12
run_server --port 10501 --server-name 'Pięciograczowy_1,_wolne_tury'   --players-count 5 --turn-duration 1000 --game-length 200 --size-x 9 --size-y 9 --explosion-radius 4 --bomb-timer 5 --initial-blocks 12
run_server --port 10502 --server-name 'Pięciograczowy_2,_wolne_tury'   --players-count 5 --turn-duration 1000 --game-length 200 --size-x 9 --size-y 9 --explosion-radius 4 --bomb-timer 5 --initial-blocks 12

run_server --port 10510 --server-name 'Pięciograczowy_0,_średnie_tury' --players-count 5 --turn-duration  400 --game-length 200 --size-x 9 --size-y 9 --explosion-radius 4 --bomb-timer 6 --initial-blocks 12
run_server --port 10511 --server-name 'Pięciograczowy_1,_średnie_tury' --players-count 5 --turn-duration  400 --game-length 200 --size-x 9 --size-y 9 --explosion-radius 4 --bomb-timer 6 --initial-blocks 12
run_server --port 10512 --server-name 'Pięciograczowy_2,_średnie_tury' --players-count 5 --turn-duration  400 --game-length 200 --size-x 9 --size-y 9 --explosion-radius 4 --bomb-timer 6 --initial-blocks 12

run_server --port 10520 --server-name 'Pięciograczowy_0,_szybkie_tury' --players-count 5 --turn-duration  100 --game-length 750 --size-x 9 --size-y 9 --explosion-radius 4 --bomb-timer 15 --initial-blocks 12
run_server --port 10521 --server-name 'Pięciograczowy_1,_szybkie_tury' --players-count 5 --turn-duration  100 --game-length 750 --size-x 9 --size-y 9 --explosion-radius 4 --bomb-timer 15 --initial-blocks 12
run_server --port 10522 --server-name 'Pięciograczowy_2,_szybkie_tury' --players-count 5 --turn-duration  100 --game-length 750 --size-x 9 --size-y 9 --explosion-radius 4 --bomb-timer 15 --initial-blocks 12


run_server --port 10600 --server-name 'Sześciograczowy_0,_wolne_tury'   --players-count 6 --turn-duration 1000 --game-length 200 --size-x 9 --size-y 9 --explosion-radius 4 --bomb-timer 5 --initial-blocks 12
run_server --port 10601 --server-name 'Sześciograczowy_1,_wolne_tury'   --players-count 6 --turn-duration 1000 --game-length 200 --size-x 9 --size-y 9 --explosion-radius 4 --bomb-timer 5 --initial-blocks 12
run_server --port 10602 --server-name 'Sześciograczowy_2,_wolne_tury'   --players-count 6 --turn-duration 1000 --game-length 200 --size-x 9 --size-y 9 --explosion-radius 4 --bomb-timer 5 --initial-blocks 12

run_server --port 10610 --server-name 'Sześciograczowy_0,_średnie_tury' --players-count 6 --turn-duration  400 --game-length 200 --size-x 9 --size-y 9 --explosion-radius 4 --bomb-timer 6 --initial-blocks 12
run_server --port 10611 --server-name 'Sześciograczowy_1,_średnie_tury' --players-count 6 --turn-duration  400 --game-length 200 --size-x 9 --size-y 9 --explosion-radius 4 --bomb-timer 6 --initial-blocks 12
run_server --port 10612 --server-name 'Sześciograczowy_2,_średnie_tury' --players-count 6 --turn-duration  400 --game-length 200 --size-x 9 --size-y 9 --explosion-radius 4 --bomb-timer 6 --initial-blocks 12

run_server --port 10620 --server-name 'Sześciograczowy_0,_szybkie_tury' --players-count 6 --turn-duration  100 --game-length 750 --size-x 9 --size-y 9 --explosion-radius 4 --bomb-timer 15 --initial-blocks 12
run_server --port 10621 --server-name 'Sześciograczowy_1,_szybkie_tury' --players-count 6 --turn-duration  100 --game-length 750 --size-x 9 --size-y 9 --explosion-radius 4 --bomb-timer 15 --initial-blocks 12
run_server --port 10622 --server-name 'Sześciograczowy_2,_szybkie_tury' --players-count 6 --turn-duration  100 --game-length 750 --size-x 9 --size-y 9 --explosion-radius 4 --bomb-timer 15 --initial-blocks 12
