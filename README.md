# Testy do zadania 2

### Testy klienta

Proszę nie traktujcie ich zbyt poważnie - sprawdzają tylko kilka prostych scenariuszy
i zdecydowanie nie gwarantują poprawności rozwiązania. Niemniej jednak coś tam testują,
więc wierzę, że mogą się okazać przydatne. Wszelkie pytania, uwagi, poprawki, własne testy mile
widziane.

**Użycie**: standardowo klonujemy do katalogu z klientem i `python3 client/test.py [-d]`.
Opcjonalna flaga `-d` pozwala zobaczyć, co wypisuje na stdout i stderr uruchamiany
podczas testów klient.

### Testy serwera

Opis jw. Użycie: `python3 server/test.py [-d]`.

### Dostępne serwery

`cat run_servers.sh`

### Docs

Przypominam, by nie zadawać pytań na forum jak nie trzeba. 
Istnieje docs z listą pytań i odpowiedzi.
Link do niego jest gdzieś na grupie fb.

### Dodawanie swoich testów

Patrz [tutaj](https://gitlab.com/mimuw-ipp-2021/testy-duze-zadanie-3)
