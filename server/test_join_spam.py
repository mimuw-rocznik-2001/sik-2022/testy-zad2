from core import Server, Client
from multiprocessing import Pool, Barrier
from formats import *


def _send_join(client):
    client.send(Join('dipsy' + str(barrier.wait())))
    client.send(Join('poo'))


def _init_send_join(barrier_):
    global barrier
    barrier = barrier_


def test_join_spam():
    client_count = 25
    players_count = 20
    s = Server(
        bomb_timer=2,
        players_count=players_count,
        turn_duration=500,
        explosion_radius=1,
        initial_blocks=10,
        game_length=6,
        server_name='testowy',
        size_x=9,
        size_y=10,
    )

    clients = [Client(s) for _ in range(client_count)]
    barrier = Barrier(client_count)
    with Pool(client_count, initializer=_init_send_join, initargs=(barrier, )) as pool:
        pool.imap_unordered(_send_join, clients)
        pool.close()
        pool.join()

    accepted_players = None
    for client in clients:
        loc_accepted_players = []
        for _ in range(players_count):
            acc_msg = client.receive()
            assert isinstance(acc_msg, AcceptedPlayer)
            loc_accepted_players.append(acc_msg.player)
        loc_accepted_players.sort()

        if accepted_players is None:
            accepted_players = loc_accepted_players
        else:
            assert loc_accepted_players == accepted_players

        started_msg = client.receive()
        assert isinstance(started_msg, GameStarted)
        started_accepted_players = list(started_msg.players.values())
        started_accepted_players.sort()
        assert started_accepted_players == accepted_players



if __name__ == '__main__':
    test_join_spam()
