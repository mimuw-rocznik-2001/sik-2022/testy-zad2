import random
import socket
import subprocess
import sys
import time
from contextlib import closing

from formats import Hello, Position, recv_server_msg, read_exactly

SERVER_PATH = '../robots-server'
TIMEOUT = 1


def find_free_port():
    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as s:
        s.bind(('', 0))
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        return s.getsockname()[1]


class Random:
    Q = 48271
    MOD = 2147483647

    def __init__(self, seed):
        self.r = seed

    def next(self):
        self.r = (self.r * self.Q) % self.MOD
        return self.r


class Server:
    def __init__(self, bomb_timer, players_count, turn_duration,
                 explosion_radius, initial_blocks, game_length, server_name,
                 size_x, size_y, seed=42):
        self.valid_hello = Hello(
            server_name,
            players_count,
            size_x,
            size_y,
            game_length,
            explosion_radius,
            bomb_timer
        )

        self.rand = Random(seed)
        self.size_x = size_x
        self.size_y = size_y

        if "-d" not in sys.argv:
            popen_kwargs = {
                'stdout': subprocess.DEVNULL,
                'stderr': subprocess.DEVNULL,
            }
        else:
            popen_kwargs = {}

        self.port = find_free_port()

        self.server = subprocess.Popen([
            SERVER_PATH,
            '-b', str(bomb_timer),
            '-c', str(players_count),
            '-d', str(turn_duration),
            '-e', str(explosion_radius),
            '-k', str(initial_blocks),
            '-l', str(game_length),
            '-n', str(server_name),
            '-p', str(self.port),
            '-s', str(seed),
            '-x', str(size_x),
            '-y', str(size_y)
        ], **popen_kwargs)
        time.sleep(0.5)

    def __del__(self):
        self.server.terminate()
        self.server.communicate()

    def next_position(self):
        return Position(
            self.rand.next() % self.size_x,
            self.rand.next() % self.size_y
        )


class Client:
    def __init__(self, server, max_packet_size=None):
        self.max_packet_size = max_packet_size

        self.socket = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        self.socket.settimeout(TIMEOUT)
        self.socket.connect(("localhost", server.port))

        assert self.receive() == server.valid_hello

    def disconnect(self):
        self.socket.close()

    def send(self, msg):
        if not self.max_packet_size:
            self.socket.sendall(msg)
        else:
            sent = 0
            while sent < len(msg):
                packet_size = random.randint(1, min(self.max_packet_size, len(msg) - sent))
                self.socket.sendall(msg[sent:sent + packet_size])
                sent += packet_size

    def receive(self):
        return recv_server_msg(self.socket)

    def should_not_receive_anything(self):
        try:
            read_exactly(self.socket, 1)
        except (TimeoutError, socket.timeout):
            pass
        else:
            assert False
