from core import Server, Client
from formats import *


def test_simple_one_client():
    player_name = 'danon'
    initial_blocks = 15
    s = Server(
        bomb_timer=2,
        players_count=1,
        turn_duration=500,
        explosion_radius=1,
        initial_blocks=initial_blocks,
        game_length=6,
        server_name='testowy',
        size_x=9,
        size_y=10,
    )
    c = Client(s)

    # Join
    c.send(Join(player_name))
    accepted_msg = c.receive()

    assert isinstance(accepted_msg, AcceptedPlayer)
    assert (accepted_msg.id == 0 or accepted_msg.id == 1)
    assert accepted_msg.player.name == player_name

    # GameStarted
    started_msg = c.receive()
    assert isinstance(started_msg, GameStarted)
    assert started_msg.players[accepted_msg.id] == accepted_msg.player

    # Turn 0
    turn_msg = c.receive()
    assert isinstance(turn_msg, Turn)
    assert turn_msg.turn == 0

    player_pos = None
    block_poss = []
    for event in turn_msg.events:
        if isinstance(event, BlockPlaced):
            block_poss.append(event.position)
        elif isinstance(event, PlayerMoved):
            assert player_pos is None
            assert event.id == accepted_msg.id
            player_pos = event.position

    assert player_pos == s.next_position()
    block_valid_poss = set()
    for _ in range(initial_blocks):
        block_valid_poss.add(s.next_position())

    assert len(block_poss) == len(block_valid_poss)
    assert set(block_poss) == block_valid_poss


if __name__ == '__main__':
    test_simple_one_client()