import sys
import traceback

from test_simple_one_client import test_simple_one_client
from test_join_spam import test_join_spam
from test_connect_during_game import test_connect_during_game
from test_one_alive_one_dead import test_one_alive_one_dead

tests = [
    test_simple_one_client,
    test_join_spam,
    test_connect_during_game,
    test_one_alive_one_dead
]

if __name__ == '__main__':
    for test in tests:
        print(test.__name__ + (' (' + test.__doc__ + ')' if test.__doc__ else '') + '... ', end='')
        sys.stdout.flush()
        try:
            test()
        except Exception as ex:
            print('ERROR')
            print('-------------------------------------------')
            print(traceback.format_exc())
        else:
            print('OK')
