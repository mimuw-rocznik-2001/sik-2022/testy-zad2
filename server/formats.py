from collections import namedtuple
from enum import IntEnum


def read_exactly(sock, n):
    buff = bytearray(n)
    pos = 0
    while pos < n:
        cr = sock.recv_into(memoryview(buff)[pos:])
        if cr == 0:
            raise EOFError
        pos += cr
    return buff


def pack_u8(val):
    return int(val).to_bytes(1, 'big')


def recv_u8(sock):
    return int.from_bytes(read_exactly(sock, 1), 'big')


def pack_u16(val):
    return int(val).to_bytes(2, 'big')


def recv_u16(sock):
    return int.from_bytes(read_exactly(sock, 2), 'big')


def pack_u32(val):
    return int(val).to_bytes(4, 'big')


def recv_u32(sock):
    return int.from_bytes(read_exactly(sock, 4), 'big')


def pack_str(s):
    b = s.encode('utf-8')
    return int(len(b)).to_bytes(1, 'big') + b


def recv_str(sock):
    len = recv_u8(sock)
    b = read_exactly(sock, len)
    return b.decode('utf-8')


class Direction(IntEnum):
    UP = 0
    RIGHT = 1
    DOWN = 2
    LEFT = 3


def pack_dir(direction):
    return pack_u8(int(direction))


Player = namedtuple('Player', 'name address')


def recv_player(sock):
    return Player(
        name=recv_str(sock),
        address=recv_str(sock)
    )


Position = namedtuple('Position', 'x y')


def recv_position(sock):
    return Position(
        x=recv_u16(sock),
        y=recv_u16(sock)
    )


# --------------------- Events -------------------

PlayerMoved = namedtuple('PlayerMoved', 'id position')


def recv_player_moved(sock):
    return PlayerMoved(
        id=recv_u8(sock),
        position=recv_position(sock)
    )


BlockPlaced = namedtuple('BlockPlaced', 'position')


def recv_block_placed(sock):
    return BlockPlaced(
        position=recv_position(sock)
    )


def recv_event(sock):
    type = recv_u8(sock)
    handlers = [
        None,
        None,
        recv_player_moved,
        recv_block_placed,
    ]
    return handlers[type](sock)


# --------------------- Server messages -------------------

Hello = namedtuple('Hello', 'server_name players_count size_x size_y ' + \
                   'game_length explosion_radius bomb_timer')


def recv_hello(sock):
    return Hello(
        server_name=recv_str(sock),
        players_count=recv_u8(sock),
        size_x=recv_u16(sock),
        size_y=recv_u16(sock),
        game_length=recv_u16(sock),
        explosion_radius=recv_u16(sock),
        bomb_timer=recv_u16(sock),
    )


AcceptedPlayer = namedtuple('AcceptedPlayer', 'id player')


def recv_accepted_player(sock):
    return AcceptedPlayer(
        id=recv_u8(sock),
        player=recv_player(sock)
    )


GameStarted = namedtuple('GameStarted', 'players')


def recv_game_started(sock):
    size = recv_u32(sock)
    res = {}
    for _ in range(size):
        id = recv_u8(sock)
        res[id] = recv_player(sock)
    return GameStarted(players=res)


Turn = namedtuple('Turn', 'turn events')


def recv_turn(sock):
    turn = recv_u16(sock)
    events = []
    events_len = recv_u32(sock)
    for _ in range(events_len):
        events += [recv_event(sock)]
    return Turn(turn, events)


def recv_server_msg(sock):
    type = recv_u8(sock)
    handlers = [
        recv_hello,
        recv_accepted_player,
        recv_game_started,
        recv_turn,
    ]
    return handlers[type](sock)


# --------------------- Client messages -------------------

def Join(name):
    return pack_u8(0) + pack_str(name)


def PlaceBomb():
    return pack_u8(1)


def PlaceBlock():
    return pack_u8(2)


def Move(direction):
    return pack_u8(3) + pack_dir(direction)
