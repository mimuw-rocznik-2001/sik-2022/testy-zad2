from core import Server, Client
from formats import *


def test_connect_during_game():
    player_name = 'Makka Pakka 🙃'
    delay = 3
    s = Server(
        bomb_timer=2,
        players_count=1,
        turn_duration=600,
        explosion_radius=1,
        initial_blocks=10,
        game_length=10,
        server_name='testowy',
        size_x=9,
        size_y=10,
    )
    player = Client(s)
    player.send(Join(player_name))
    assert isinstance(player.receive(), AcceptedPlayer)

    started = player.receive()
    assert isinstance(started, GameStarted)

    turns = [player.receive() for _ in range(delay)]
    for turn in turns:
        assert isinstance(turn, Turn)

    late = Client(s)
    assert late.receive() == started
    for turn in turns:
        assert late.receive() == turn



if __name__ == '__main__':
    test_connect_during_game()