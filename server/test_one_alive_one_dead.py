from core import Server, Client
from formats import *


def _test_one_alive_one_dead(game_length, disconnect):
    s = Server(
        bomb_timer=2,
        players_count=2,
        turn_duration=1,
        explosion_radius=1,
        initial_blocks=10,
        game_length=game_length,
        server_name='testowy',
        size_x=9,
        size_y=10,
    )
    alive = Client(s)
    dead = Client(s)

    dead.send(Join('anon'))
    assert isinstance(dead.receive(), AcceptedPlayer)
    assert isinstance(alive.receive(), AcceptedPlayer)

    alive.send(Join('danon'))
    assert isinstance(alive.receive(), AcceptedPlayer)
    assert isinstance(alive.receive(), GameStarted)

    if disconnect:
        dead.disconnect()

    for t in range(game_length + 1):
        turn_msg = alive.receive()
        assert isinstance(turn_msg, Turn)
        assert turn_msg.turn == t
        if t > 0:
            assert len(turn_msg.events) == 0


def test_one_alive_one_dead():
    """it will take 30 seconds"""
    _test_one_alive_one_dead(game_length=200, disconnect=True)
    _test_one_alive_one_dead(game_length=30000, disconnect=False)


if __name__ == '__main__':
    test_one_alive_one_dead()
